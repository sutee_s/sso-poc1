import React, { Component } from 'react'
import { Route, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'redux'

import { doAuthoriztionMethod } from '../../lib/services/authorization'
import { authorizationRequest } from '../../actions/authorization'

class AuthRoute extends Component {
  state = {
    isLogin: false,
  }

  doRefreshToken = () => {
    return true
  }

  componentDidMount() {
    const args = {
      authorizationUrl: 'http://localhost:8082/auth/realms/poc/protocol/openid-connect/auth',
      redirectUrl: location.href,
      clientId: 'cca',
      doRefreshToken: this.doRefreshToken,
    }
    const paramsString = this.props.location.search
    const queryParams = new URLSearchParams(paramsString)
    const code = queryParams.get('code')
    if (code) {
      const { origin, pathname } = location
      const redirectUrl = `${origin}${pathname}`
      const params = {
        code,
        client_id: 'cca',
        redirect_uri: redirectUrl,
      }
      this.props.getTokenRequest(params)
      setTimeout(() => location.replace(redirectUrl), 1000)
    } else if (doAuthoriztionMethod(args)) {
      this.setState({ isLogin: true })
    }
  }

  render() {
    const { component } = this.props
    if (this.state.isLogin) {
      return <Route component={component} {...this.props} />
    }
    return null
  }
}

const mapDispatchToProps = dispatch => ({
  getTokenRequest: params => dispatch(authorizationRequest(params)),
})

export default compose(
  connect(null, mapDispatchToProps),
  withRouter
)(AuthRoute)
