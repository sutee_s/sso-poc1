import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { logoutRequest } from '../../../actions/authorization'
import { getUserBasicInfo } from '../../../lib/services/authorization'
import './index.scss'

class Home extends Component {
  static propTypes = {
    logoutRequest: PropTypes.func.isRequired,
  }

  doLogout = () => {
    const userInfo = getUserBasicInfo()
    const params = {
      client_id: 'cca',
      refresh_token: userInfo.refreshToken,
    }
    this.props.logoutRequest(params)
    location.reload()
  }

  componentDidMount() {
    console.log('information', getUserBasicInfo())
  }

  render() {
    return (
      <div className="home-container">
        <img src='/static/images/logo.png' />
        <h1 className="red-text">CCA</h1>
        <div>
          <a href="http://localhost:7001">CCA</a>
          <b> | </b>
          <a href="http://localhost:7002">CIS</a>
          <b> | </b>
          <a href="#" onClick={this.doLogout}>Logout</a>
        </div>
        <img className="img-app" src='/static/images/app.png' />
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  logoutRequest: params => dispatch(logoutRequest(params))
})

export default connect(null, mapDispatchToProps)(Home)
