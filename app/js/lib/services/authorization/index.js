import moment from 'moment'

// http://localhost:8082/auth/realms/poc/protocol/openid-connect/auth?redirect_uri=http%3A%2F%2Fwww.example.com&response_type=code&client_id=cca

const _parseJWT = (jwtToken) => {
  const base64Url = jwtToken.split('.')[1]
  const base64 = base64Url.replace('-', '+').replace('_', '/')
  return JSON.parse(window.atob(base64))
}

const _checkTokenExpire = (token) => {
  return false
}

const _checkRefreshTokenExpire = (refreshToken) => {
  return false
}

const _doRedirectAuthk = ({ redirectUrl, authorizationUrl, clientId }) => {
  const encodedUrl = encodeURIComponent(redirectUrl)
  const completeUrl = `${authorizationUrl}?redirect_uri=${encodedUrl}&client_id=${clientId}&response_type=code`
  window.location.replace(completeUrl)
}

const _doAuthorization = ({
  token,
  refreshToken,
  redirectUrl,
  authorizationUrl,
  clientId,
  doRefreshToken, // method for refresh token => maybe action
}) => {
  if (token) {
    if (!_checkTokenExpire(token)) {
      return true
    } else if (refreshToken && !_checkRefreshTokenExpire(refreshToken)) {
      doRefreshToken(refreshToken)
    } else {
      _doRedirectAuthk({ redirectUrl, authorizationUrl, clientId })
    }
  } else {
    _doRedirectAuthk({ redirectUrl, authorizationUrl, clientId })
  }
  return false
}

export const doAuthoriztionMethod = ({
  authorizationUrl,
  redirectUrl,
  clientId,
  doRefreshToken, // method for refresh token => maybe action
}) => {
  const token = localStorage.getItem('access_token')
  const refreshToken = localStorage.getItem('refresh_token')
  return _doAuthorization({
    token,
    refreshToken,
    redirectUrl,
    authorizationUrl,
    clientId,
    doRefreshToken,
  })
}

export const setToken = ({ token, refreshToken }) => {
  localStorage.setItem('access_token', token)
  localStorage.setItem('refresh_token', refreshToken)
}

export const removeToken = () => {
  localStorage.removeItem('access_token')
  localStorage.removeItem('refresh_token')
}

export const getUserBasicInfo = () => {
  const token = localStorage.getItem('access_token')
  const refreshToken = localStorage.getItem('refresh_token')
  const tokenDetail = _parseJWT(token)
  const refreshTokenDetail = _parseJWT(refreshToken)

  return {
    token,
    refreshToken,
    tokenDetail,
    refreshTokenDetail,
  }
}
