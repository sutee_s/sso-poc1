import {
  makeActionCreator,
  AUTHORIZATION_REQUEST,
  AUTHORIZATION_SUCCESS,
  AUTHORIZATION_FAILURE,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  LOGOUT_FAILURE,
} from './action-types'

export const authorizationRequest = makeActionCreator(AUTHORIZATION_REQUEST, 'params')
export const authorizationSuccess = makeActionCreator(AUTHORIZATION_SUCCESS, 'payload')
export const authorizationFailure = makeActionCreator(AUTHORIZATION_FAILURE, 'error')

export const logoutRequest = makeActionCreator(LOGOUT_REQUEST, 'params')
export const logoutSuccess = makeActionCreator(LOGOUT_SUCCESS)
export const logoutFailure = makeActionCreator(LOGOUT_FAILURE, 'error')
