import { combineEpics } from 'redux-observable'

import { AUTHORIZATION_REQUEST, LOGOUT_REQUEST } from '../actions/action-types'
import { authorizationSuccess, logoutSuccess } from '../actions/authorization'
import { setToken, removeToken } from '../lib/services/authorization'

const authorizationRequestEpic = (action$, store, { callStaticAPI }) =>
  action$.ofType(AUTHORIZATION_REQUEST)
    .mergeMap(action =>
      callStaticAPI({
        url: 'http://localhost:8082/auth/realms/poc/protocol/openid-connect/token',
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: {
          grant_type: 'authorization_code',
          ...action.params,
        }
      })
        .map(result => result.response)
        .do((result) => {
          const { access_token: token, refresh_token: refreshToken } = result
          setToken({ token, refreshToken })
        })
        .map(result => authorizationSuccess(result))
    )

const logoutRequestEpic = (action$, store, { callStaticAPI }) =>
  action$.ofType(LOGOUT_REQUEST)
    .mergeMap(action =>
      callStaticAPI({
        url: 'http://localhost:8082/auth/realms/poc/protocol/openid-connect/logout',
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: action.params
      })
        .map(result => result.response)
        .do(() => {
          removeToken()
        })
        .map(() => logoutSuccess())
    )

export default combineEpics(
  authorizationRequestEpic,
  logoutRequestEpic,
)
