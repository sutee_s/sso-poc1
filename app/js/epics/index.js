import { combineEpics } from 'redux-observable'

import authorizationEpic from './authorization-epic'
import errorEpic from './error-epic'
import pingPongEpic from './ping-pong-epic'

export default combineEpics(
  authorizationEpic,
  errorEpic,
  pingPongEpic,
)
